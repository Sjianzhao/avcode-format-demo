#include "../include/libavcodec/avcodec.h"     // 编解码
#include "../include/libavformat/avformat.h"   // 解封装
#include "../include/libavutil/opt.h"
#include "../include/libswscale/swscale.h"     // 格式转换
#include <stdio.h>
#include <stdlib.h>




/*
 @ 1 -- : 初始化相对应的组件包
 @ 2 -- : 打开相对应的文件，获取相关参数
 @ 3 -- : 获取媒体文件的流
 @ 4 -- : 寻找视频流
 @ 5 -- ：选取相对应的解码器
 @ 6 -- ：设置解码器参数
 @ 7 -- ：解码出来的包进行解码
 @ 8 -- ：解码后的视频帧进行播放
*/


void save_frame(AVFrame *pFrame, int width, int heigh, int iFrame)
{
    FILE *pFile;
    char filename[32];

    sprintf(filename, "frame%d.ppm", iFrame);
    pFile = fopen(filename, "wb+");
    if(pFile == NULL)
    {
        perror("open file error \n");
        return 0;
    }
    // 写PPM的header
    fprintf(pFile, "P6\n%d %d\n255\n", width,heigh);
    // 写数据
    for(int y = 0; y < heigh; y++)
    {
        fwrite(pFrame->data[0] + y * pFrame->linesize[0], 1, width * 3 ,pFile);
    }
    fclose(pFile);
}

/*
 @ func: 解封装，解码，YUV转成RGB并保存前50帧图片
*/
int func01(int argc, char ** argv)
{

    AVFormatContext *pFormatCtx;
    AVCodecContext *pCodecCtx;
    AVCodec *pCodec;
    AVStream *pStream;
    AVPacket *packet;
    AVFrame *pFrame;
    AVFrame *pFrameRGB;
    uint8_t *buffer;    /* 视频帧的缓存区指针 */
    int video_stream = 0;
    int img_size = 0;
    int frame_finish = 0;
    if(argc < 2)
    {
        perror("input para less 2 \n");
        exit(0);
    }
    /* 初始化编解码器组件 */
    av_register_all();  
    /* 初始化网络包;支持网络流输入 */
    avformat_network_all();
    /* 创建封装上下文 */
    pFormatCtx = avformat_alloc_context();
    /* 打开媒体文件 根据后缀获取头信息*/
    if(avformat_open_input(pFormatCtx,argv[1],NULL,NULL) < 0)
    {
        perror("avformat_open_init fail \n");
        return -1;
    }
    if(avformat_find_stream_info(pFormatCtx,NULL) < 0)
    {
        return -1;
    }
    av_dump_format(pFormatCtx,-1,argv[1],0);

    video_stream = -1;
    for(int i = 0; i < pFormatCtx->nb_streams;i++)
    {
        if(pFormatCtx->streams[i]->codec->codec_type = AVMEDIA_TYPE_VIDEO)
        {
            video_stream = i;    // 记录视频流在哪个AVStreams中
            break;
        }
    }
    if(video_stream == -1)
    {
        return -1;
    }
    pCodecCtx = pFormatCtx->streams[video_stream]->codec;    // 获取解码参数
    /* 根据解码参数获取解码器 */
    pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
    if(pCodec == NULL)
    {
        perror("get decodec error \n");
        exit(0);
    }
    /* 打开解码器 */
    if(avcodec_open2(pCodecCtx, pCodec, NULL) < 0)
    {
        return -1;
    }

    /* 为视频帧分配空间*/
    pFrame = av_frame_alloc();
    if(pFrame == NULL)
    {
        return -1;
    }
    pFrameRGB = av_frame_alloc();
    if(pFrameRGB == NULL)
    {
        return -1;
    }
    img_size = avpicture_get_size(AV_PIX_FMT_BGR24, pCodecCtx->width, pCodecCtx->height);
    buffer = av_malloc(img_size * sizeof(uint8_t));
    avpicture_fill((AVPicture *)pFrameRGB, buffer, AV_PIX_FMT_BGR24, pCodecCtx->width, pCodecCtx->height);

    /* 读取帧数据 */
    int i = 0;
    while(av_read_frame(pFormatCtx,&packet) >= 0)
    {
        if(packet->stream_index = video_stream)
        {
            /* 逐帧解码 */
            avcodec_decode_video2(pCodecCtx,pFrame,&frame_finish,&packet);
            if(frame_finish)
            {
                /* YUV 转换成 RGB */
                struct SwsContext *img_sws_ctx = NULL;
                img_sws_ctx = sws_getCachedContext(img_sws_ctx,pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt, \
                                                    pCodecCtx->width,pCodecCtx->height,AV_PIX_FMT_BGR24,SWS_BICUBIC, NULL, NULL, NULL);
                if (img_sws_ctx)
                {
                    exit(1);
                }
                /* 格式转换 */
                sws_scale(img_sws_ctx, pFrame->data, pFrame->linesize, 0, pCodecCtx->height, pFrameRGB->data,pFrameRGB->linesize);
                if(i++ < 50)
                    save_frame(pFrameRGB,pCodecCtx->width,pCodecCtx->height,i);
            }
        }
        av_free_packet(&packet);
    }

    av_free(buffer);
    av_free(pFrameRGB);
    av_free(pFrame);
    avcodec_close(pCodecCtx);
    avformat_close_input(pFormatCtx);
    return 0;
}


/*

 @func: ffmpeg结构分析
 URLProtocol
 URLContext
 AVInputFormat
 AVFormatContext
 AVCodec
 AVCodeContext
 AVFormatContext
 AVStreams
 AVPacket
 AVFrame

*/


/* 输入的像素数据读取完成后调用此函数，用于输出编码器中剩余的AVPacket */
int flush_encoder(AVFormatContext *fmt_ctx,unsigned int stream_index){
	int ret;
	int got_frame;
	AVPacket enc_pkt;
	if (!(fmt_ctx->streams[stream_index]->codec->codec->capabilities & AV_CODEC_CAP_DELAY))
		return 0;
	while (1) {
		enc_pkt.data = NULL;
		enc_pkt.size = 0;
		av_init_packet(&enc_pkt);
		//编码一帧视频。即将AVFrame（存储YUV像素数据）编码为AVPacket（存储H.264等格式的码流数据）。
		ret = avcodec_encode_video2 (fmt_ctx->streams[stream_index]->codec, &enc_pkt,
			NULL, &got_frame);
		av_frame_free(NULL);
		if (ret < 0)
			break;
		if (!got_frame){
			ret=0;
			break;
		}
		printf("Flush Encoder: Succeed to encode 1 frame!\tsize:%5d\n",enc_pkt.size);
		/* mux encoded frame */
		ret = av_write_frame(fmt_ctx, &enc_pkt);
		if (ret < 0)
			break;
	}
	return ret;
}




/* YUV 编码成 H.264 */
int func02(int argc, char ** argv)
{
/**
*************** FFMPEG视频编码流程 *******************
* 01、av_register_all()：注册FFmpeg所有编解码器;
* 02、avformat_alloc_output_context2()：初始化输出码流的AVFormatContext;
* 03、avio_open()：打开输出文件;
* 04、av_new_stream()：创建输出码流的AVStream;
* 05、avcodec_find_encoder()：查找编码器;
* 06、avcodec_open2()：打开编码器;
* 07、avformat_write_header()：写文件头(对于某些没有文件头的封装格式，不需要此函数。比如说MPEG2TS);
* 08、不停地从码流中提取出YUV数据，进行编码;
*    avcodec_encode_video2()：编码一帧视频。即将AVFrame(存储YUV像素数据)编码为AVPacket(存储H.264等格式的码流数据);
*    av_write_frame()：将编码后的视频码流写入文件;
* 09、flush_encoder()：输入的像素数据读取完成后调用此函数。用于输出编码器中剩余的AVPacket;
* 10、av_write_trailer()：写文件尾(对于某些没有文件头的封装格式，不需要此函数。比如说MPEG2TS);
*/
    AVFormatContext *p_format_ctx;
    AVOutputFormat *p_output_format;        /* 说白了，就是设置这个结构体里面的相关参数，要完整的编码参数之后，才能进行编码 */
    AVStream *p_stream;
    AVCodecContext *p_code_ctx;
    AVCodec *p_codec;
    AVPacket *p_pack;
    AVFrame *p_frame;
    const char * in_file = "./ds_480_272.yuv";
    FILE *fp = fopen(in_file,"rb+");
    int in_w = 480,in_h = 272;
    int framenum = 100;            // 要编码的总帧数
    const char *out_file = "ds.h264";


    av_register_all();

    p_format_ctx = avformat_alloc_context();
    if(p_format_ctx == NULL)
    {
        perror("p_format_ctx error\n");
        exit(0);
    }
    /* av_guess_format 这是一个决定视频输出时封装方式的函数，其中有三个参数，写任何一个参数，都会自动匹配相应的封装方式。 */
    p_output_format = av_guess_format(NULL, out_file, NULL);
    p_format_ctx->oformat = p_output_format;
    /* 打开输出文件 */
    if(avio_open(&p_format_ctx->pb,out_file,AVIO_FLAG_READ_WRITE) < 0)
    {
        printf("open file fail \n");
        return -1;
    }
    /* 创建输出码流的stream */
    p_stream = avformat_new_stream(p_format_ctx,0);
    p_stream->time_base.den = 25;
    p_stream->time_base.num = 1;
    if(p_stream == NULL)
    {
        return -1;
    }
    /* 设置编码参数 */
    p_code_ctx = p_stream->codec;
    p_code_ctx->codec_id = p_output_format->audio_codec;
    p_code_ctx->codec_type = AVMEDIA_TYPE_VIDEO;
    p_code_ctx->pix_fmt = AV_PIX_FMT_YUV420P;
    p_code_ctx->time_base.num = 1;
    p_code_ctx->time_base.den = 25;
    p_code_ctx->width = in_w;
    p_code_ctx->height = in_h;
    p_code_ctx->bit_rate = 400000;
    p_code_ctx->gop_size = 250;
    p_code_ctx->qmin = 10;
    p_code_ctx->qmax = 51;

    p_code_ctx->max_b_frames = 3;

    /* 设置选项 */
    AVDictionary *p_param = 0;
    //H.264
	if(p_code_ctx->codec_id == AV_CODEC_ID_H264) {
		av_dict_set(&p_param, "preset", "slow", 0);
		av_dict_set(&p_param, "tune", "zerolatency", 0);
	}
	//H.265
	if(p_code_ctx->codec_id == AV_CODEC_ID_H265){
		av_dict_set(&p_param, "preset", "ultrafast", 0);
		av_dict_set(&p_param, "tune", "zero-latency", 0);
	}
    /* av_dump_format()是一个手工调试的函数，能使我们看到pFormatCtx->streams里面有什么内容。*/
    av_dump_format(p_format_ctx, 0, out_file, 1);
    p_codec = avcodec_find_encoder(p_code_ctx->codec_id); // 查找编码器
    avcodec_open2(p_format_ctx, p_codec,&p_param); // 打开编码器
    int p_frame_size = 0;
    char * frame_buff = NULL;
    p_frame = av_frame_alloc();
    p_frame_size = avpicture_get_size(p_code_ctx->pix_fmt,p_code_ctx->width,p_code_ctx->height);
    frame_buff = (uint8_t *)av_malloc(p_frame_size);
    avpicture_fill((AVPicture *) p_frame, frame_buff, p_code_ctx->pix_fmt, p_code_ctx->width, p_code_ctx->height);
    // 写文件头（对于某些没有文件头的封装格式，不需要此函数。比如说MPEG2TS）。
	avformat_write_header(p_format_ctx,NULL);
    int y_size = 0;
    int framecnt = 0;
    /* 分配内存存储原始帧 */
    av_new_packet(p_pack,p_frame_size);
    y_size = p_code_ctx->width * p_code_ctx->height;

    /* 一帧一帧循环处理*/
    for(int i = 0; i < framenum; i++)
    {
        // yuv 420   y 4  u 1  v 1  
        if(fread(frame_buff,1,y_size*3/2,in_file) <= 0)
        {
            return -1;
        } else if(feof(in_file)){   // 判断文件是否结束
            break;
        }
        p_frame->data[0] = frame_buff;      // Y
        p_frame->data[1] = frame_buff + y_size;            // U
        p_frame->data[2] = frame_buff + y_size * 5 / 4;    // V
        /* pTS */
        p_frame->pts = i;
        int got_pic = 0;
        // 编码 
        int ret = avcodec_encode_video2(p_code_ctx, p_pack,p_frame, &got_pic); 
        if(ret < 0){
			printf("Failed to encode! \n");
			return -1;
		}
		if (got_pic==1){
			printf("Succeed to encode frame: %5d\tsize:%5d\n",framecnt,p_pack->size);
			framecnt++;
			p_pack->stream_index = p_stream->index;
			ret = av_write_frame(p_format_ctx, p_pack); // 将编码后的视频码流写入文件,
			av_free_packet(p_pack); // free
		}
    }   
    // 处理剩余的AVPacket
    int ret = flush_encoder(p_format_ctx,0);
    if (ret < 0) {
		printf("Flushing encoder failed\n");
		return -1;
	}
    // 写文件尾(对于某些没有文件头的封装格式，不需要此函数。比如说MPEG2TS)
	av_write_trailer(p_format_ctx); 
    // Clean
	if (p_stream){
		avcodec_close(p_stream->codec);
		av_free(p_stream);
		av_free(frame_buff);
	}
	avio_close(p_format_ctx->pb);
	avformat_free_context(p_format_ctx);
	fclose(in_file);
    return 0;
}






















