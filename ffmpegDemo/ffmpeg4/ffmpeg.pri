SOURCES += \
    $$PWD/ffmpegwidget.cpp


HEADERS += \
    $$PWD/ffmpegwidget.h

strLib = lib
strInclude = include

#表示arm平台构建套件
contains(QT_ARCH, arm) {
strInclude = include
}

!android {
INCLUDEPATH += $$PWD/$$strInclude
}

win32 {
LIBS += -L$$PWD/$$strLib/$$winlib -lavcodec -lavfilter -lavformat -lswscale -lavutil -lswresample -lavdevice -lpostproc
}

#请自行替换
!android {
unix:!macx {
LIBS += -L$$PWD/linuxlib/ -lavfilter -lavformat -lavdevice -lavcodec -lswscale -lavutil -lswresample -lavdevice -lpthread -lm -lz -lrt -ldl
}}














