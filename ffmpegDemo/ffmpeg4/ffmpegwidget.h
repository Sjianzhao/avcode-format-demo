﻿#ifndef FFMPEGWIDGET_H
#define FFMPEGWIDGET_H


//必须加以下内容,否则编译不能通过,为了兼容C和C99标准
#ifndef INT64_C
#define INT64_C
#define UINT64_C
#endif

//引入ffmpeg头文件
extern "C" {
#include "libavutil/opt.h"
#include "libavutil/time.h"
#include "libavutil/frame.h"
#include "libavutil/pixdesc.h"
#include "libavutil/avassert.h"
#include "libavutil/imgutils.h"
#include "libavutil/ffversion.h"
#include "libavcodec/avcodec.h"
#include "libswscale/swscale.h"
#include "libswresample/swresample.h"
#include "libavformat/avformat.h"
#include "libavfilter/avfilter.h"

#ifdef ffmpegdevice
#include "libavdevice/avdevice.h"
#endif

#ifndef gcc45
#include "libavutil/hwcontext.h"
#include "libavutil/hwcontext_qsv.h"
#endif
}

#include "qdatetime.h"
#include <QWidget>
#include <QLabel>
#include <QtGui>
#pragma execution_character_set("utf-8")

#define TIMEMS          qPrintable(QTime::currentTime().toString("HH:mm:ss zzz"))
#define TIME            qPrintable(QTime::currentTime().toString("HH:mm:ss"))
#define QDATE           qPrintable(QDate::currentDate().toString("yyyy-MM-dd"))
#define QTIME           qPrintable(QTime::currentTime().toString("HH-mm-ss"))
#define DATETIME        qPrintable(QDateTime::currentDateTime().toString("yyyy-MM-dd HH:mm:ss"))
#define STRDATETIME     qPrintable(QDateTime::currentDateTime().toString("yyyy-MM-dd-HH-mm-ss"))
#define STRDATETIMEMS   qPrintable(QDateTime::currentDateTime().toString("yyyy-MM-dd-HH-mm-ss-zzz"))



namespace Ui {

    class ffmpegwidget;

}

class ffmpegwidget : public QWidget
{
    Q_OBJECT
public:
    explicit ffmpegwidget(QWidget *parent = nullptr);
    ~ffmpegwidget();

public:
    void init_lib();
    void file_read(QString path);
    void file_reformat();
    void file_recodec();
    void av_pack_to_frame();
protected:
    void paintEvent(QPaintEvent *);
private:
    QImage image;
    AVFormatContext	*pFormatCtx;
    int				i, videoindex;
    AVCodecContext	*pCodecCtx;
    AVCodec			*pCodec;
    AVFrame	*pFrame,*pFrameYUV;
    AVPacket *packet;
    QImage *img_convert_ctx;
    int screen_w,screen_h;

};





#endif // FFMPEGWIDGET_H
