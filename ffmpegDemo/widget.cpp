﻿#include "widget.h"
#include "ui_widget.h"

#include <QLabel>
#include <QFileDialog>
#include <QMessageBox>
#include <QDebug>
Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
    ui->fileName->setText("请选取播放文件");
    img_play = new ffmpegwidget();
}

Widget::~Widget()
{
    delete ui;
}

/*
 @func:选取文件
*/
void Widget::on_selectFile_clicked()
{
    // 打开文件夹
    QString fileName = QFileDialog::getOpenFileName(
            this,
            tr("open a file."),
            "F:/",
            tr("video files(*.avi *.mp4 *.wmv);;images(*.png *jpeg *bmp)"));
        if (fileName.isEmpty()) {
            QMessageBox::warning(this, "Warning!", "Failed to open the video!");
        }
        else {
           qDebug() << "file path is :  " <<fileName;
            ui->fileName->setText(fileName);
        }
}
/*
 @func:播放按钮
 @将视屏文件取帧，进行贴图
*/
void Widget::on_playButton_clicked()
{
    img_play->file_read(ui->fileName->text());

}
/*
 @func:暂停按钮
 @ 暂时先不做  ；
*/
void Widget::on_pauseButton_clicked()
{

}
