﻿#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include "ffmpegwidget.h"


QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_selectFile_clicked();

    void on_playButton_clicked();

    void on_pauseButton_clicked();

private:
    Ui::Widget *ui;
    ffmpegwidget *img_play;
};
#endif // WIDGET_H
